function bestEconomyFinderInSuperOver(deliveriesData){

    const superOverData = deliveriesData.filter((row) => {
        return row.is_super_over == 1;
    });

    const bowlerData = {}

    
    superOverData.forEach(row => {
        const bowler = row.bowler
        const wide_ball = Number(row.wide_runs)
        const bye_run = Number(row.legbye_runs) + Number(row.bye_runs)
        const no_ball = Number(row.noball_runs)
        const total_run = Number(row.total_runs)
        const penalty_run = Number(row.penalty_runs)

        const bowler_run = total_run - (penalty_run + bye_run)
        
        if(!bowlerData[bowler]){
            if(!(no_ball || wide_ball)){
                bowlerData[bowler] = {
                    run : bowler_run,
                    ball : 0
                }
            }
        }
        else{
            if(!no_ball || wide_ball){
                bowlerData[bowler].run  += bowler_run
                bowlerData[bowler].ball++
            }
        }

    });

    for (const bowler in bowlerData) {
        const run = bowlerData[bowler].run;
        const ball = bowlerData[bowler].ball;

        const economy = ((run * 6.0) / ball).toFixed(2);

        bowlerData[bowler].economy = parseFloat(economy);
    }

    return bowlerData
}

module.exports = bestEconomyFinderInSuperOver