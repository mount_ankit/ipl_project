function matchesPlayedPerYear(matchesData){
    const matchesYears = {}

    matchesData.forEach((match)=>{
        let year = match.season
        if(!matchesYears[year]){
            matchesYears[year] = 1
        }
        else{
            matchesYears[year]++;
        }
    });

    return matchesYears;

}

module.exports = matchesPlayedPerYear;
