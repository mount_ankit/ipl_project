


function tossAndMatchWinnerFinder(matchesData){

    const tossAndMatchWinner = {}

    matchesData.forEach((row) => {
        const toss_winner = row.toss_winner
        const match_winner = row.winner

        if(toss_winner === match_winner){

        if(!tossAndMatchWinner[toss_winner]){
            tossAndMatchWinner[toss_winner] = 1
        }
        else{
            tossAndMatchWinner[toss_winner]++
        }


        }    
    });
    
    return tossAndMatchWinner;

}


module.exports = tossAndMatchWinnerFinder