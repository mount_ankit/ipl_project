const { count } = require("console");

function highestDismissalFinder(deliveriesData){

    const DismissalData = deliveriesData.filter((row) => {
        return row.dismissal_kind !== '' && row.dismissal_kind !== 'run out';
    });
    
    const NumberOfDissmisal = {}

    DismissalData.forEach(row => {
        const batsman = row.batsman
        const bowler = row.bowler

        if (NumberOfDissmisal[batsman]) {
            if(NumberOfDissmisal[batsman][bowler]){
                NumberOfDissmisal[batsman][bowler]++
            }
            else{
                NumberOfDissmisal[batsman][bowler] = 1
            }
        }
        else{
            NumberOfDissmisal[batsman] = {};
            NumberOfDissmisal[batsman][bowler] = 1;
        }

        

    });

    let highestCount = 0;
    let highestPlayer = '';

    for (const batsman in NumberOfDissmisal) {
        for (const bowler in NumberOfDissmisal[batsman]) {
            if (NumberOfDissmisal[batsman][bowler] > highestCount) {
                highestCount = NumberOfDissmisal[batsman][bowler];
                highestPlayer = batsman;
            }
        }
    }

    return { player: highestPlayer, dismissals: highestCount };    ;


}

module.exports = highestDismissalFinder;
