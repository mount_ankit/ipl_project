function findingOvers(deliveriesData, minval, maxval) {
    const ballOfBowler = {};
  
    deliveriesData.forEach((row) => {
      const bowler = row.bowler;
      const matchId = Number(row.match_id);
      const wideBalls = Number(row.wide_runs);
      const noBalls = Number(row.noball_runs);


      if (matchId >= minval && matchId <= maxval) {
        if (!ballOfBowler[bowler]) {
          ballOfBowler[bowler] = 0;
        }
        if(!(wideBalls || noBalls)){
            ballOfBowler[bowler]+=1;
        }
      }
    });

  
    for (const bowler in ballOfBowler) {
      const totalBalls = ballOfBowler[bowler];
      const overs = Math.floor(totalBalls / 6);
      const balls = (totalBalls % 6) / 10;
      const totalOvers = overs + balls;
      ballOfBowler[bowler] = totalOvers;
    }
  
    return ballOfBowler;
  }
  
  function findingRunsGiven(deliveriesData, minval, maxval) {
    const runsOfBowler = {};
  
    deliveriesData.forEach((row) => {
      const bowler = row.bowler;
      const totalRun = Number(row.total_runs);
      const byeRun = Number(row.legbye_runs) + Number(row.bye_runs) + Number(row.penalty_runs);
      const bowlerRun = totalRun - byeRun;
  
      if (row.match_id >= minval && row.match_id <= maxval) {
        if (!runsOfBowler[bowler]) {
          runsOfBowler[bowler] = bowlerRun;
        } else {
          runsOfBowler[bowler] += bowlerRun;
        }
      }
    });
  
    return runsOfBowler;
  }
  
  function calculateEconomy(run, over) {
    if (over === 0) {
      return 0;
    }
    return (run / over).toFixed(2);
  }
  
  function economicalBowlerFinder(deliveriesData, matchesData) {
    let minval = 1e6;
    let maxval = -1e7;
  
    matchesData.filter((match_year) => {
      if (match_year.season == 2016) {
        minval = Math.min(minval, match_year.id);
        maxval = Math.max(maxval, match_year.id);
      }
    });
  
    const runsOfBowler = findingRunsGiven(deliveriesData, minval, maxval);
    const oversOfBowler = findingOvers(deliveriesData, minval, maxval);
  
    const economyOfBowlers = [];
  
    for (const bowler in runsOfBowler) {
      if (oversOfBowler[bowler] !== undefined) {
        const economy = calculateEconomy(runsOfBowler[bowler], oversOfBowler[bowler]);
        economyOfBowlers.push({ bowler, economy });
      }
    }
  
    // Sort the array by economy in ascending order
    economyOfBowlers.sort((a, b) => a.economy - b.economy);
  
    const top10EconomicalBowlers = economyOfBowlers.slice(0, 10);
  
    return top10EconomicalBowlers;
  }
  
  module.exports = economicalBowlerFinder;
  