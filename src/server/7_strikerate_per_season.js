const { log } = require("console");

function RunAndBallFinder(deliveriesData, matchesData, player_name, year) {
    const BattersData = {};

    deliveriesData.forEach((row) => {
        const season = matchesData.find((match) => match.id == row.match_id)?.season;

        const batter = row.batsman;
        const batsman_runs = Number(row.batsman_runs);
        const wideBalls = Number(row.wide_runs);
        const noBalls = Number(row.noball_runs);

        if (batter === player_name && season === year) {
            if (!BattersData[batter]) {
                BattersData[batter] = {
                    runs: 0,
                    balls: 0,
                };
            }

            if (!noBalls && !wideBalls) {
                BattersData[batter].runs += batsman_runs;
                BattersData[batter].balls += 1;
            }
        }
    });

    return BattersData;
}

function StrikeRateFinder(deliveriesData, matchesData, player_name, year) {
    const runAndBalls = RunAndBallFinder(deliveriesData, matchesData, player_name, year);

    for (const batter in runAndBalls) {
        const run = runAndBalls[batter].runs;
        const ball = runAndBalls[batter].balls;

        const StrikeRate = ((run / ball) * 100).toFixed(2);

        runAndBalls[batter] = StrikeRate;
    }

    return runAndBalls;
}

function StrikeRateForEachYear(deliveriesData, matchesData, player_name) {
    const StrikeRateforYear = {};

    deliveriesData.forEach((row) => {
        const season = matchesData.find((match) => match.id == row.match_id)?.season;

        if (season) {
            if (!StrikeRateforYear[season]) {
                StrikeRateforYear[season] = StrikeRateFinder(deliveriesData, matchesData, player_name, season);
            }
        }
    });

    
    return StrikeRateforYear;
}

module.exports = StrikeRateForEachYear;
