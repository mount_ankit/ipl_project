function highestManOfTheMatchFinder(matchesData) {
    const ManOfTheMatchPerSeason = {};

    matchesData.forEach((row) => {
        const man_of_match = row.player_of_match;
        const season = Number(row.season);

        if (!ManOfTheMatchPerSeason[season]) {
            ManOfTheMatchPerSeason[season] = {};
        }

        if (!ManOfTheMatchPerSeason[season][man_of_match]) {
            ManOfTheMatchPerSeason[season][man_of_match] = 1;
        } else {
            ManOfTheMatchPerSeason[season][man_of_match]++;
        }
    });

    for (const season in ManOfTheMatchPerSeason) {
        const players = ManOfTheMatchPerSeason[season];

        const sortedPlayers = Object.keys(players).sort(
            (a, b) => players[b] - players[a]
        );

        const highestPlayer = sortedPlayers[0];

        ManOfTheMatchPerSeason[season] = {
            player: String(highestPlayer), // Convert to string
            count: Number(players[highestPlayer]), // Convert to number
        };
    }

    

    return ManOfTheMatchPerSeason;
}

module.exports = highestManOfTheMatchFinder;
