function matchesWonPerTeamPerSeason(matchesData){
    
    const winnerPerSeason ={}

    matchesData.forEach((match) => {
        let year = match.season
        let winner = match.winner

        if(!winnerPerSeason[year]){

            winnerPerSeason[year] = { [winner] : 1 }
        }
        else{

            if(!winnerPerSeason[year][winner]){
                winnerPerSeason[year][winner] = 1
            }
            else{
                winnerPerSeason[year][winner]++
            }

        }

    });

    return winnerPerSeason

}

module.exports = matchesWonPerTeamPerSeason;