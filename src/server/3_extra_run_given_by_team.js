function extraRunGivenPerTeam(deliveriesData, matchesData) {
    let minval = 1e6;
    let maxval = -1e7;

    const range = matchesData.filter(match_year => {
        if (match_year.season == 2016) {
            minval = Math.min(minval, match_year.id);
            maxval = Math.max(maxval, match_year.id);
        }
        return true; 
    });

    const extraRunsByTeam = {};

    deliveriesData.forEach(row => {
        let id = row.match_id;
        let team = row.bowling_team;
        let extra_run = Number(row.extra_runs);
        


        if (id >= minval && id <= maxval) {

            if (!extraRunsByTeam[team]) {
                extraRunsByTeam[team] = extra_run;
            } else {
                extraRunsByTeam[team] += extra_run;
            }
        }
    });
    
    return extraRunsByTeam;
}

// Call the function and use the result
module.exports = extraRunGivenPerTeam;

