const fs = require('fs');
const csv = require('csv-parser');
const pathMatches = './src/data/matches.csv';
const pathDeliveries = './src/data/deliveries.csv';
const matchesPlayedPerYear = require('./src/server/1_matches_played_per_year.js')
const matchesWonPerTeamPerSeason = require('./src/server/2_matches_won_per_team_per_season.js')
const extraRunGivenPerTeam = require('./src/server/3_extra_run_given_by_team.js')
const economicalBowlerFinder = require('./src/server/4_top_10_economical_bowler.js')
const tossAndMatchWinnerFinder = require('./src/server/5_toss_and_match_winner.js')
const highestManOfTheMatchFinder = require('./src/server/6_highest_matches_per_season.js')
const StrikeRateFinder = require('./src/server/7_strikerate_per_season.js')
const highestDismissalFinder= require('./src/server/8_highest_time_dissmised.js')
const bestEconomyFinderInSuperOver = require('./src/server/9_best_economy_in_superover.js')

const matchesData = [];
const deliveriesData = [];

fs.createReadStream(pathMatches).pipe(csv()).on('data',(matches)=>{
    matchesData.push(matches);
    
}).on('end',()=>{
    fs.createReadStream(pathDeliveries).pipe(csv()).on('data',(deliveries)=>{
    deliveriesData.push(deliveries);

    }).on('end',()=>{

        const matchesvsyears = matchesPlayedPerYear(matchesData)
        const winnersPerSeason = matchesWonPerTeamPerSeason(matchesData)
        const extraRunByTeams = extraRunGivenPerTeam(deliveriesData,matchesData)
        const econnomyBallers = economicalBowlerFinder(deliveriesData,matchesData)
        const tossAndMatchwinner = tossAndMatchWinnerFinder(matchesData)
        const ManOfTheMatchPeryear = highestManOfTheMatchFinder(matchesData)
        const StrikeRate = StrikeRateFinder(deliveriesData,matchesData,'MS Dhoni')
        const DismissalVsPlayer = highestDismissalFinder(deliveriesData)
        const superOverBestBowler = bestEconomyFinderInSuperOver(deliveriesData)

        fs.writeFileSync('./src/public/output/problem1.json', JSON.stringify(matchesvsyears, null, 2));
        fs.writeFileSync('./src/public/output/problem2.json', JSON.stringify(winnersPerSeason, null, 2));
        fs.writeFileSync('./src/public/output/problem3.json', JSON.stringify(extraRunByTeams, null, 2));
        fs.writeFileSync('./src/public/output/problem4.json', JSON.stringify(econnomyBallers, null, 2));
        fs.writeFileSync('./src/public/output/problem5.json', JSON.stringify(tossAndMatchwinner, null, 2));
        fs.writeFileSync('./src/public/output/problem6.json', JSON.stringify(ManOfTheMatchPeryear, null, 2));
        fs.writeFileSync('./src/public/output/problem7.json', JSON.stringify(StrikeRate, null, 2));
        fs.writeFileSync('./src/public/output/problem8.json', JSON.stringify(DismissalVsPlayer, null, 2));
        fs.writeFileSync('./src/public/output/problem9.json', JSON.stringify(superOverBestBowler, null, 2));
    })
})